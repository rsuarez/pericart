$(document).ready(function(){
	
//IMAGE FORM

$(".rotation").InputSpinner({
	decrementButton: "<img src='/img/tourne_g.png' style='width: 15px;'>", 
	incrementButton: "<img src='/img/tourne_d.png' style='width: 15px;'>"
	})


$(".cx").InputSpinner({
	decrementButton: "<img src='/img/fleche_g.png' style='width: 15px;'>", 
	incrementButton: "<img src='/img/fleche_d.png' style='width: 15px;'>"
	});

$(".cy").InputSpinner({
	decrementButton: "<img src='/img/fleche_b.png' style='width: 15px;'>", 
	incrementButton: "<img src='/img/fleche_h.png' style='width: 15px;'>" 
	});

$(".ew").InputSpinner({
	decrementButton: "<img src='/img/fleche_g.png' style='width: 15px;'>", 
	incrementButton: "<img src='/img/fleche_d.png' style='width: 15px;'>" 
	});

$(".eh").InputSpinner({
	decrementButton: "<img src='/img/fleche_b.png' style='width: 15px;'>", 
	incrementButton: "<img src='/img/fleche_h.png' style='width: 15px;'>" 
	});


//MOUSE COORDINATE

var meridian = document.getElementById('meridien');
var coordobox = document.getElementById('coordsys');

var mouseLonLat = new ol.control.MousePosition({
	coordinateFormat: function(coord) {
		lon = coord[0] - parseFloat(meridian.value);
        lat = coord[1];
        modifiedCoordinate = [lon, lat];
		return ol.coordinate.toStringHDMS(modifiedCoordinate);
		},
	projection: 'EPSG:4326',
    className: 'custom-mouse-position',
    target: document.getElementById('mouse-position'),
    undefinedHTML: '&nbsp;'
    });	
	
var mouseLL = new ol.control.MousePosition({
	coordinateFormat: function(coordinate) {
        lon = coordinate[0] - parseFloat(meridian.value);
        lat = coordinate[1];
        modifiedCoordinate = [lat, lon];
        return ol.coordinate.format(modifiedCoordinate, "{x}, {y}", 4);
		},
	projection: 'EPSG:4326',
    className: 'custom-mouse-position',
	target: document.getElementById('mouse-position'),
	undefinedHTML: '&nbsp;'
	});
		
var mouseGrade = new ol.control.MousePosition({
	coordinateFormat: function(coordinate) {
        lon = coordinate[0];
        lat = coordinate[1];
		lon = (lon - parseFloat(meridian.value))*1.1111;
		lat = lat*1.1111;
        modifiedCoordinate = [lat, lon]
        return ol.coordinate.format(modifiedCoordinate, "{x}, {y}", 4);
		},
	projection: 'EPSG:4326',
    className: 'custom-mouse-position',
	target: document.getElementById('mouse-position'),
	undefinedHTML: '&nbsp;'
	});


//POPUP SET ID

var popup = new ol.Overlay.Popup ({
	popupClass: "shadow",
	closeBox: true,
	positioning: 'auto',
	autoPan: true,
	autoPanAnimation: { duration: 250 }
	});
	

//LAYER OSM

 var baseLayers = new ol.layer.Group({
    title: 'Fonds de carte',
    openInLayerSwitcher: true,
	 layers: [
      new ol.layer.Tile({
        title: "Topo",
        baseLayer: true,
        visible: false,
        source: new ol.source.Stamen({ layer: 'terrain' })
      }),
      new ol.layer.Tile({
        title: "OSM",
        baseLayer: true,
        source: new ol.source.OSM(),
        visible: true
      })
    ]
  });


//MAP

var map = new ol.Map ({
    target: 'map',
    view: new ol.View ({
		projection: 'EPSG:3857',
		zoom: 3,
		center: ol.proj.fromLonLat([0,35]),
		extent: ol.proj.transformExtent([-180, -90, 300, 90], "EPSG:4326", 'EPSG:3857')
		}),
    controls: ol.control.defaults({ "attribution": false }),
	layers: baseLayers,
	overlays: [popup]
    });

$('#mouse-position').appendTo(
	$('.ol-overlaycontainer')
	);
 
var cswitch = new ol.control.LayerSwitcher();

map.addControl(cswitch);


//VECTOR
   
var vector = new ol.layer.Vector({
    name: 'Vecteur',
    source: new ol.source.Vector({ features: new ol.Collection() }),
    zIndex : 2,
	style: function (fiture) { 
		if (fiture.getGeometry().getType() == "Polygon") {				
			return polstyle;
			}	
		else if (fiture.getGeometry().getType() == "MultiPolygon") {
			return polstyle;
			}	
		else if (fiture.getGeometry().getType() == "Point") {
			return pstyle;
			}
		else if (fiture.getGeometry().getType() == "Circle") {
			return cstyle;
			}
		else {
			return lstyle;
			}		
		}
	});

map.addLayer(vector);

var vecsource = vector.getSource();

var sourcemesure = new ol.source.Vector();

var vectormesure = new ol.layer.Vector({
	source: sourcemesure,
	style: new ol.style.Style({
    fill: new ol.style.Fill({
		color: 'rgba(255, 255, 255, 0.2)'
		}),
    stroke: new ol.style.Stroke({
		color: '#ffcc33',
		width: 2
		}),
    image: new ol.style.Circle({
		radius: 7,
		fill: new ol.style.Fill({
        color: '#ffcc33'
				})
			})
		})
	});

var sourceimage = new ol.source.Vector({ features: new ol.Collection() });

var vectorimage = new ol.layer.Vector({
	source: sourceimage,
	style: new ol.style.Style({
    fill: new ol.style.Fill({
		color: 'rgba(255, 255, 255, 0.2)'
		}),
    stroke: new ol.style.Stroke({
		color: '#fbff7c',
		width: 2
		}),
    image: new ol.style.Circle({
		radius: 7,
		fill: new ol.style.Fill({
			color: '#fbff7c'
				})
			})
		})
	});


// IMAGE MODAL
  
var imodal = document.getElementById("myModal2");


// VECTEUR MODAL
  
var gmodal = document.getElementById("myModal1");


//BBOX MODAL

var boxmodal = document.getElementById("myModal3");
var boxmodal2 = document.getElementById("myModal4");

// GRATICULE

var stylegrat = new ol.style.Style();
stylegrat.setStroke (new ol.style.Stroke({ color:"#b3b3b3", width:1 }));
stylegrat.setText (new ol.style.Text({stroke: new ol.style.Stroke({ color:"#595959", width:0 }), fill: new ol.style.Fill({ color:"#595959" })}));
	
	
//PROJ LIST

proj4.defs("perigrat_0", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=0 +no_defs");
proj4.defs("perigrat_2.337222", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=2.337222 +no_defs");
proj4.defs("perigrat_4.883611", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=4.883611 +no_defs");
proj4.defs("perigrat_4.380556", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=4.380556 +no_defs");
proj4.defs("perigrat_23.716389", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=23.716389 +no_defs");
proj4.defs("perigrat_13.398611", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=13.398611 +no_defs");
proj4.defs("perigrat_4.367975", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=4.367975 +no_defs");
proj4.defs("perigrat_12.575625", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=12.575625 +no_defs");
proj4.defs("perigrat_-18.05", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=-18.05 +no_defs");
proj4.defs("perigrat_-17.662778", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=-17.662778 +no_defs");
proj4.defs("perigrat_24.954722", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=24.954722 +no_defs");
proj4.defs("perigrat_28.980556", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=28.980556 +no_defs");
proj4.defs("perigrat_-3.687911", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=-3.687911 +no_defs");
proj4.defs("perigrat_19.722917", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=19.722917 +no_defs");
proj4.defs("perigrat_30.328358", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=30.328358 +no_defs");
proj4.defs("perigrat_12.452333", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=12.452333 +no_defs");
proj4.defs("perigrat_4.496111", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=4.496111 +no_defs");
proj4.defs("perigrat_18.058278", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=18.058278 +no_defs");
proj4.defs("perigrat_21.011667", "+proj=longlat  +ellps=WGS84 +datum=WGS84  +from_greenwich=21.011667 +no_defs");

ol.proj.proj4.register(proj4);

var graticule = new ol.control.Graticule({ step: 1, stepCoord: 5, formatCoord :function(c){ return c.toFixed(0)+"°"}});
graticule.setStyle(stylegrat);
map.addControl(graticule);

$('#meridien').on('change', function() {
	map.removeControl(graticule);
	if (coordobox.value =="grad") {
		graticule = new ol.control.Graticule({ step: 0.9, stepCoord: 5,projection: "perigrat_"+this.value, formatCoord: function(c){ return (c*1.1111).toFixed(1)+" Gr"}});
		graticule.setStyle(stylegrat);
		map.addControl(graticule);
		}
	else {
		graticule = new ol.control.Graticule({ step: 1, stepCoord: 5,projection: "perigrat_"+this.value, formatCoord: function(c){ return c.toFixed(1)+"°"}});
		graticule.setStyle(stylegrat);
		map.addControl(graticule);
		}
	});

	
// MOUSE POSITION INTERACTION

function coordcontrol (){
	map.removeControl(mouseLL);
	map.removeControl(mouseLonLat);
	map.removeControl(mouseGrade);
	}

map.addControl(mouseLonLat);

$('#decitype').hide();
$('#gradtype').hide();
$("#sexatype").show();
	
coordobox.addEventListener('change', function() {
	if (this.value == 'deci') {
		coordcontrol();
		map.removeControl(graticule);
		graticule = new ol.control.Graticule({ step: 1, stepCoord: 5,projection: "perigrat_"+$('#meridien').val(), formatCoord: function(c){ return c.toFixed(1)+"°"}})
		graticule.setStyle(stylegrat);
		map.addControl(graticule);
		map.addControl(mouseLL);
		$("#sexatype").hide();
		$('#gradtype').hide();
		$("#decitype").show();
		}
	 else if(this.value == 'grad') {
		coordcontrol();
		map.removeControl(graticule);
		graticule = new ol.control.Graticule({ step: 0.9, stepCoord: 5,projection: "perigrat_"+$('#meridien').val(), formatCoord: function(c){ return (c*1.1111).toFixed(1)+" Gr"}})
		graticule.setStyle(stylegrat);
		map.addControl(graticule);
		map.addControl(mouseGrade);
		$("#sexatype").hide();
		$('#gradtype').show();
		$("#decitype").hide();
		}		
	else if(this.value == 'sexa') {
		coordcontrol();
		map.removeControl(graticule);
		graticule = new ol.control.Graticule({ step: 1, stepCoord: 5,projection: "perigrat_"+$('#meridien').val(), formatCoord: function(c){ return c.toFixed(0)+"°"}})
		graticule.setStyle(stylegrat);
		map.addControl(graticule);
		map.addControl(mouseLonLat);
		$("#sexatype").show();
		$('#gradtype').hide();
		$("#decitype").hide();	
		}
	})

	
//AJOUT POINTS	
		
$("#godec").click(function(){
	var laty = parseFloat($("#declat").val());
	var lonx = parseFloat($("#declon").val()) + parseFloat(meridian.value);
	if (laty >= -90 && laty <= 90 && lonx >= -180 && lonx <= 180) {
		var xy84 = new ol.proj.fromLonLat([lonx, laty], "EPSG:3857");
		vecsource.addFeatures([new ol.Feature({type: 'Point', geometry: new ol.geom.Point(xy84)})]);
		} else {bootbox.alert("valeur erronée");}
	})
		
$("#gograd").click(function(){
	var laty = parseFloat($("#grlat").val()) / 1.1111;
	var lonx = (parseFloat($("#grlon").val()) / 1.1111) + parseFloat(meridian.value);
	if (laty >= -95 && laty <= 95 && lonx >= -200 && lonx <=200) {
		var xy84 = new ol.proj.fromLonLat([lonx, laty], "EPSG:3857");
		vecsource.addFeatures([new ol.Feature({type: 'Point',geometry: new ol.geom.Point(xy84)})]);
		} else {bootbox.alert("valeur erronée");}
	})
	
$("#gosexa").click(function(){
	if (parseInt($("#minlat").val()) < 60 && parseInt($("#deglat").val()) <= 90 && parseInt($("#seclat").val()) < 60 && parseInt($("#minlat").val()) >= 0 && parseInt($("#deglat").val()) >= 0 && parseInt($("#seclat").val()) >= 0 && parseInt($("#minlon").val()) < 60 && parseInt($("#deglon").val()) <= 180 && parseInt($("#seclon").val()) < 60	&& parseInt($("#minlon").val()) >= 0 && parseInt($("#deglon").val()) >= 0 && parseInt($("#seclon").val()) >= 0 ) 
	{	var convlat = (parseFloat($("#minlat").val()/60 ) + parseFloat($("#deglat").val()) +  parseFloat($("#seclat").val()/ 3600) ).toFixed(4);
		if ($("#equa").val() == 'sud') {
			convlat= convlat * -1;
				} 
		if ($("#merid").val() == 'ouest') {
			var convlon = (parseFloat($("#minlon").val()/60 ) + parseFloat($("#deglon").val()) +  parseFloat($("#seclon").val()/ 3600)).toFixed(4);
			convlon = convlon * -1;
			convlon = (convlon + parseFloat(meridian.value)).toFixed(4);
				}
		if ($("#merid").val() == 'est') {
			var convlon = (parseFloat($("#minlon").val()/60 ) + parseFloat($("#deglon").val()) +  parseFloat($("#seclon").val()/ 3600) + parseFloat(meridian.value)).toFixed(4);
				}	
		var xy = new ol.proj.fromLonLat([convlon, convlat], "EPSG:3857");
		vecsource.addFeatures([new ol.Feature({type: 'Point', geometry: new ol.geom.Point(xy)})]);	
		} else {bootbox.alert("valeur erronée");}
	})
	

//VECTOR STYLE

var tstyle = new ol.style.Style({
	text: new ol.style.Text({
		font: '16px Calibri,sans-serif',
		overflow: true,
		fill: new ol.style.Fill({
			color: "#9933ff"
			}),
		stroke: new ol.style.Stroke({
			color: "#ffffff",
			width: 5
			})
		})
	});

var polstyle = [new ol.style.Style({
	fill: new ol.style.Fill({
		color: 'rgba(255, 194, 102, 0.2)'
			}),
	stroke: new ol.style.Stroke({
		color: "#0099ff",
		width: 1
			}),
	image: new ol.style.Circle({
		radius: 7,
		fill: new ol.style.Fill({
			color: "#0099ff"
			})
		})
	}),
	new ol.style.Style({
	image: new ol.style.Circle({
		radius: 4,
		fill: new ol.style.Fill({
			color: '#0099ff'
			})
       }),
    geometry: function(fitpol) {
        var coordinates = fitpol.getGeometry().getCoordinates()[0];
        return new ol.geom.MultiPoint(coordinates);
		}
	})];

var polstylid = [new ol.style.Style({
	fill: new ol.style.Fill({
		color: 'rgba(255, 194, 102, 0.2)'
			}),
	stroke: new ol.style.Stroke({
		color: "#0099ff",
		width: 1
			}),
	image: new ol.style.Circle({
		radius: 7,
		fill: new ol.style.Fill({
			color: "#0099ff"
			})
		})
	}),
	tstyle,
	new ol.style.Style({
	image: new ol.style.Circle({
		radius: 4,
		fill: new ol.style.Fill({
			color: '#0099ff'
			})
       }),
    geometry: function(fitpol) {
        var coordinates = fitpol.getGeometry().getCoordinates()[0];
        return new ol.geom.MultiPoint(coordinates);
		}
	})];

var pstyle = new ol.style.Style({
	fill: new ol.style.Fill({
		color: 'rgba(255, 255, 255, 0.2)'
		}),
	stroke: new ol.style.Stroke({
		color: "#ff3300",
		width: 1
	  }),
	image: new ol.style.Circle({
		radius: 4,
		fill: new ol.style.Fill({
			color: "#ff3300"
			})
		})  
    });

var lstyleold = new ol.style.Style({
	fill: new ol.style.Fill({
		color: 'rgba(255, 255, 255, 0.2)'
		}),
	stroke: new ol.style.Stroke({
		color: "#009933",
		width: 2
		}),
	image: new ol.style.Circle({
		radius: 7,
		fill: new ol.style.Fill({
			color: "#009933"
			})
		})  	  
    });

var lstyle = [new ol.style.Style({
	fill: new ol.style.Fill({
		color: 'rgba(255, 255, 255, 0.2)'
		}),
	stroke: new ol.style.Stroke({
		color: "#009933",
		width: 2
		}),
	image: new ol.style.Circle({
		radius: 7,
		fill: new ol.style.Fill({
			color: "#009933"
			})
		})  	  
    }),
	new ol.style.Style({
	image: new ol.style.Circle({
		radius: 4,
		fill: new ol.style.Fill({
			color: "#009933"
			})
       }),
    geometry: function(fitpol) {
        var coordinates = fitpol.getGeometry().getCoordinates();
        return new ol.geom.MultiPoint(coordinates);
		}
	})]	
	
var cstyle = new ol.style.Style({
	fill: new ol.style.Fill({
		color: 'rgba(255, 255, 255, 0.2)'
		}),
	stroke: new ol.style.Stroke({
		color: "#993399",
		width: 1
		}),
    image: new ol.style.Circle({
		radius: 5,
		fill: new ol.style.Fill({
			color: "#993399"
			})
		}) 
    });
	
	
//DESSIN

var drawi = new ol.interaction.Draw({ 
    source: vecsource,
    type: "Polygon"
    });

drawi.on('drawend', function(evt1){
	var feature = evt1.feature;
	feature.setId('pol' + feature.ol_uid);
	if ($("#idcheck").is(":checked")) {
		feature.setStyle(
			function (fiture) {
				tstyle.getText().setText(String(fiture.getId()));
				return polstylid;
					}
				)
			}

	});
	
var drawpoint = new ol.interaction.Draw({
    source: vecsource,
    type: "Point"
    });		
	
drawpoint.on('drawend', function(evt2){
	var feature = evt2.feature;
	feature.setId('p' + feature.ol_uid);
	});	
			
var drawline = new ol.interaction.Draw({
    source: vecsource,
    type: "LineString"
    });		
	
drawline.on('drawend', function(evt3){
	var feature = evt3.feature;
	feature.setId('l' + feature.ol_uid);
	});
	
var drawcircle = new ol.interaction.Draw({
    source: vecsource,
    type: "Circle"
    });
	
drawcircle.on('drawend', function(evt4){
	var feature = evt4.feature;
	feature.setId('c' +feature.ol_uid);
	});

var drawmesure = new ol.interaction.Draw({
    source: vectormesure.getSource(),
    type: "LineString",
    style: new ol.style.Style({
		fill: new ol.style.Fill({
			color: 'rgba(255, 255, 255, 0.2)'
			}),
		stroke: new ol.style.Stroke({
			color: 'rgba(0, 0, 0, 0.5)',
			lineDash: [10, 10],
			width: 2
			}),
		image: new ol.style.Circle({
			radius: 5,
			stroke: new ol.style.Stroke({
				color: 'rgba(0, 0, 0, 0.7)'
				}),
			fill: new ol.style.Fill({
				color: 'rgba(255, 255, 255, 0.2)'
				})
			})
		})
  });
	

//DESSIN ORTHO

geometryFunction = ol.interaction.Draw.createBox();
	
var drawortho = new ol.interaction.Draw({
	source : vecsource,
    type: "Circle",
    geometryFunction: geometryFunction
    });
	
drawortho.on('drawend', function(evt5){
	var feature = evt5.feature;
	feature.setId('rec' + feature.ol_uid);
	if ($("#idcheck").is(":checked")) {
		feature.setStyle(
			function (fiture) {
				tstyle.getText().setText(String(fiture.getId()));
				return polstylid;
					}
				)
			}
	});


//LOAD IMAGE

$("#paramimage").hide();

var drawortho2 = new ol.interaction.Draw({
	source : vectorimage.getSource(),
    type: "Circle",
    geometryFunction: geometryFunction,
    style: new ol.style.Style({
    fill: new ol.style.Fill({
		color: 'rgba(255, 255, 255, 0.2)'
			}),
    stroke: new ol.style.Stroke({
		color: '#ffcc33',
		width: 2
			}),
    image: new ol.style.Circle({
		radius: 7,
		fill: new ol.style.Fill({
			color: '#ffcc33'
				})
			})
		})
	});

drawortho2.on('drawend', function(evt6){
	var feature = evt6.feature;
	feature.setId("polimg");
	imgext = feature.getGeometry().getExtent();
	xmin = imgext[0];
	ymin = imgext[1];
	xmax = imgext[2];
	ymax = imgext[3];
	imodal.style.display = "block";
	var span = document.getElementsByClassName("close2")[0];
	span.onclick = function() {
		map.removeLayer(vectorimage);
		imodal.style.display = "none";
		drawortho2.setActive(false);
		}
	window.onclick = function(event) {
		if (event.target == imodal) {
			imodal.style.display = "none"; 
			map.removeLayer(vectorimage);
				}
			}

	$(document).on('change','.btn-file :file', function() {
		var input = $(this),
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
			});
		
	$('.btn-file :file').on('fileselect', function(event, label) {
		var input = $(this).parents('.input-group').find(':text');
		log = label;
		input.val(log)
		});

	function readURL(input) {
		if (input.files && input.files[0]) {
		    var reader = new FileReader();
		    reader.onload = function (e) {
				var image = new Image();
				image.src = e.target.result;
				image.onload = function () {
					var resy = this.height;
					var resx = this.width;
					var scalex = resx / resy;
					var sy = (ymax - ymin) / resy;
					var sx = ((ymax - ymin)*scalex) / resx;
					xmax2 = xmin + (resx*sx);
					imcenter = [((xmax2 - xmin) / 2) + xmin, ((ymax - ymin) / 2) + ymin];
		            if (typeof geoimg !== 'undefined')  {map.removeLayer(geoimg);}  	    
					geoimg = new ol.layer.GeoImage({
						name : "image",
						source: new ol.source.GeoImage({
							url: e.target.result,
							imageScale: [sx,sy],
							imageCenter : imcenter,
							zIndex : 1
							})
						});
					map.addLayer(geoimg);
					$("#paramimage").show();
					$("#x").val(imcenter[0]);
					$("#y").val(imcenter[1]);
					$("#w").val(sx);
					$("#h").val(sy);
					$("#angle").val(0);
					drawortho2.setActive(false);
					$("#fimage").val('');
					imodal.style.display = "none";
					map.removeLayer(vectorimage);
					}    
		        }
			reader.readAsDataURL(input.files[0]);
			}
		}	
		
		$("#imgInp").change(function(){
			readURL(this);
			this.value = null;
			});
	});

$(".rotation").on("change", resetRot);

function resetRot () {
    geoimg.getSource().setRotation($("#angle").val()*Math.PI/180);
	}

$(".cx").on("change", resetCx);

function resetCx () {
	var x = Number($("#x").val());
    var y = Number($("#y").val());
    geoimg.getSource().setCenter([x,y]);
	}

$(".cy").on("change", resetCy);

function resetCy () {
	var x = Number($("#x").val());
	var y = Number($("#y").val());
    geoimg.getSource().setCenter([x,y]);
	}

$(".ew").on("change", resetEw);

function resetEw () {
	var sx = Number($("#w").val());
	if ($("#propcheck").prop("checked") == true) {
			$("#h").val(sx);
			var sy = sx;
			} else {var sy = Number($("#h").val());}
	geoimg.getSource().setScale([sx,sy]);
	}

$(".eh").on("change", resetEh);

function resetEh () {
	var sy = Number($("#h").val());
	if ($("#propcheck").prop("checked") == true) {
			$("#w").val(sy);
			var sx = sy
			} else {
			var sx = Number($("#w").val());}
	geoimg.getSource().setScale([sx,sy]);
	}
	
	
//UPLOAD IMAGE

$("#goimg").click(function(){
	interactcontrol();
	vectorimage.getSource().forEachFeature(function(rmlayer) {
		vectorimage.getSource().removeFeature(rmlayer);
		})
	map.addLayer(vectorimage);	
	drawortho2.setActive(true);
	}); 

$("#rmimg").click(function(){
	if (typeof geoimg !== 'undefined')  {map.removeLayer(geoimg); }
	vectorimage.getSource().forEachFeature(function(rmlayer) {
		vectorimage.getSource().removeFeature(rmlayer);
		})
	imgext = [];
	xmin = [];
	ymin = [];
	xmax = [];
	ymax = [];
	imcenter =[];
	$("#paramimage").hide();
	});


// INTERACTION

var selectInteraction = new ol.interaction.Select({
vectorClass: ol.layer.VectorImage,
	style: new ol.style.Style({
		stroke: new ol.style.Stroke({
		color: "#c133ff",
		width: 3
			}),
		fill: new ol.style.Fill({
			color: 'rgba(255,255,255,0.5)'
			}),
		image: new ol.style.Circle({
			radius: 5,
			fill: new ol.style.Fill({
			color: "#c133ff"
				})
			}) 
		})
	});
	
var selectpop = new ol.interaction.Select({
vectorClass: ol.layer.VectorImage,
	style: new ol.style.Style({
		stroke: new ol.style.Stroke({
		color: "#c133ff",
		width: 3
			}),
		fill: new ol.style.Fill({
			color: 'rgba(255,255,255,0.5)'
			}),
		image: new ol.style.Circle({
			radius: 5,
			fill: new ol.style.Fill({
			color: "#c133ff"
				})
			}) 
		})
	});
	
var selectMulti = new ol.interaction.Select({
vectorClass: ol.layer.VectorImage,
	style: new ol.style.Style({
		stroke: new ol.style.Stroke({
		color: "#c133ff",
		width: 3
			}),
		fill: new ol.style.Fill({
			color: 'rgba(255,255,255,0.5)'
			}),
		image: new ol.style.Circle({
			radius: 5,
			fill: new ol.style.Fill({
			color: "#c133ff"
				})
			}) 
		})
	});

var selectTable = new ol.interaction.Select({
vectorClass: ol.layer.VectorImage,
	style: new ol.style.Style({
		stroke: new ol.style.Stroke({
		color: "#c133ff",
		width: 3
			}),
		fill: new ol.style.Fill({
			color: 'rgba(255,255,255,0.5)'
			}),
		image: new ol.style.Circle({
			radius: 5,
			fill: new ol.style.Fill({
			color: "#c133ff"
				})
			}) 
		})
	});

var itransform = new ol.interaction.Transform({translate: false, translateFeature: false, stretch: false, scale: false, keepApsectRatio: false});

var modi = new ol.interaction.Modify({ 
	features: selectInteraction.getFeatures()
	});
	
	
//CHROME CONSOLE

modi.on('modifyend',function(e){ if(e.features && e.features.getArray().length) {console.log("feature id is",e.features.getArray()[0].getGeometry().getCoordinates()[0]);}
	});
	
var featureDrager = new ol.interaction.Translate({
	vectorClass: ol.layer.VectorImage
	});


// SNAP

var snap = new ol.interaction.Snap({
	source: vecsource
	});

var snapi = new ol.interaction.SnapGuides({ 
    vectorClass: ol.layer.VectorImage
    });	
	

//INTERACTIONS

map.addInteraction(selectpop);
map.addInteraction(selectMulti);
map.addInteraction(drawortho);
map.addInteraction(drawortho2);
map.addInteraction(drawi);
map.addInteraction(drawpoint);
map.addInteraction(drawline);
map.addInteraction(drawcircle);
map.addInteraction(modi);
map.addInteraction(selectInteraction);
map.addInteraction(featureDrager);
snapi.setDrawInteraction(drawi);
snapi.setDrawInteraction(drawortho);
snapi.setDrawInteraction(drawline);
snapi.setModifyInteraction(modi);
map.addInteraction(snapi);
map.addInteraction(snap);
map.addInteraction(itransform);
map.addInteraction(drawmesure);
map.addInteraction(selectTable);
	
function interactcontrol () {
	selectTable.setActive(false);
	drawmesure.setActive(false);
	selectMulti.setActive(false);
	selectpop.setActive(false);
	drawortho.setActive(false);
	drawortho2.setActive(false);
	drawi.setActive(false);
	drawpoint.setActive(false);
	drawline.setActive(false);
	drawcircle.setActive(false);
	modi.setActive(false);
	selectInteraction.setActive(false);
	featureDrager.setActive(false);
	snap.setActive(false);
	$("#union").prop("disabled",true);
	$("#multi").prop("disabled",true);
	$("#efface").prop("disabled",true);
	selectInteraction.getFeatures().clear();
	selectMulti.getFeatures().clear();
	selectpop.getFeatures().clear();	
	itransform.setActive(false);
	if ($(".ibouton").not(".btn-outline-secondary")) {$(".ibouton").addClass("btn-outline-secondary"); }
	if($(".ol-overlay-container")) {$(".ol-overlay-container").remove();}
	}
	
interactcontrol();
snapi.setActive(false);

//GUIDES

$("#snapcheck").click(function(){
	if($(this).is(":checked")) {
		snapi.setActive(true);
		}
	else {snapi.setActive(false);}
	})
	
//CHANGE ID ONCLICK

$("#addid").click(function(){
	interactcontrol();
	$("#addid").removeClass("btn-outline-secondary");
	selectpop.setActive(true);
	var closer = document.getElementById('popup-closer');
	selectpop.getFeatures().on(['add'], function(e){
		var feature = e.element;
		var content = '<input type="text" id="idfg" class="form-control" placeholder="ID" name="idfg">';
		var aa = feature.getGeometry().getExtent();
		var oo = ol.extent.getCenter(aa);
		popup.show(oo, content); 	
		})
	selectpop.getFeatures().on(['remove'], function(e) {
		popup.hide(); 
		})
	$(".closeBox").click(function() {
		var nexid = document.getElementById('idfg').value;
		if (selectpop.getFeatures().getLength() > 0 && nexid.length > 0) {
		selectpop.getFeatures().item(0).setId(nexid);
		selectpop.getFeatures().item(0).setProperties({'ID_CM':''+ nexid});
		selectpop.getFeatures().clear();}
		});
	})


//TABLE INTERACTION
   
$(function() {
	
	$(".idtab").on('hide.bs.collapse', function () {
		$("#tabcontenu").html('');
		$('#optab').prop("disabled",false);
		selectTable.getFeatures().clear();
		selectTable.setActive(false);
		$("#selectunion").prop("disabled", false);
		})

	$(".idtab").on('show.bs.collapse', function () {
		var tablo;
		interactcontrol();
		selectTable.setActive(true);
		selectTable.getFeatures().clear();
		$("#selectunion").prop("disabled", false);
		var ltab = 0;
		$("#tabcontenu").append('<div class="toolbar"></div><table class="table table-striped table-sm" data-toggle="table" data-toolbar=".toolbar" id="datab" style="width: 98%;"><thead><tr><th style="width: 10%">#</th><th style="width: 20%">ID_FG</th></tr></thead><tbody id="databod"></tbody></table>');
		vecsource.forEachFeature( function(fitable) {
			if ( fitable.getGeometry().getType() == "Polygon" || fitable.getGeometry().getType() == "MultiPolygon") {
				ltab=ltab+1;
				fitable.setProperties({'rtab':''+ (ltab - 1)});
				$("#databod").append('<tr><td>'+fitable.get('rtab')+'</td><td>'+fitable.getId()+'</td></tr>');
				}
			})	 
			 
		$('#datab').Tabledit({
			editButton: false,
			deleteButton: false,
			hideIdentifier: false,
			columns: {
				identifier: [0, '#'],
				editable: [[1, 'ID_FG']]
				}
			});

		$("#datab").on("click", "td", function () {
			selectTable.getFeatures().clear();
			var col = ($(this).index());
			if(col === 1 ) {
				var parentTr = $(this).closest('tr');
				var firstCellContent  = parentTr.find('td:eq(1)').text();
				var hightable = vecsource.getFeatureById(firstCellContent);
				if (hightable !== null) {
					var selected_collection = selectTable.getFeatures();
					selected_collection.push(hightable);
					}
				}
			});
			
		selectTable.on('select', function(eventp) {	
			if (eventp.selected[0]) {
				$("#datab td:nth-child(2)").each(function () {
					if ($(this).text() === eventp.selected[0].getId()) {
						$('td').css("border", "0px solid");
						$(this).css("border", "2px solid #c133ff");
						}
					});
				}
			})
		
		$('td').click(function(){
			$('td').css("border", "0px solid");
			})
		
		$("th").hover(function(){
			$(this).css("cursor", "pointer");
			});
		
		$('th').click(function(){
		var table = $(this).parents('table').eq(0)
		var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
		this.asc = !this.asc
		if (!this.asc){rows = rows.reverse()}
			for (var i = 0; i < rows.length; i++){table.append(rows[i])}
			})
		function comparer(index) {
			return function(a, b) {
				var valA = getCellValue(a, index), valB = getCellValue(b, index)
				return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB)
				}
			}
		function getCellValue(row, index){ return $(row).children('td').eq(index).text()}

		$('#optab').prop("disabled",true); 
	  
		$("#rectab").click(function(){
			selectTable.getFeatures().clear();
			$("#datab td:nth-child(2)").each(function () {
				$(this).css("color", "#212529");
				})
			vecsource.forEachFeature(function(tabrec) {
				$("#datab tr").each(function () {
					if ($(this).find("td:eq(0)").text() === tabrec.get('rtab')) {
						tabrec.setId($(this).find("td:eq(1)").text());
						tabrec.setProperties({'ID_CM':''+ $(this).find("td:eq(1)").text()});
						}
					})
				})
			})  
	
		function getDuplicateArrayElements(arr){
			var sorted_arr = arr.slice().sort();
			var results = [];
			for (var i = 0; i < sorted_arr.length - 1; i++) {
				if (sorted_arr[i + 1] === sorted_arr[i]) {
					results.push(sorted_arr[i]);
					}
				}
			return results;
			}
	
		$("#duptab").click(function(){
			var nowtabdata = [];
			selectTable.getFeatures().clear();
			$("#datab tr").each(function () {
				nowtabdata.push($(this).find("td:eq(1)").text());
				})
			var tabdup = getDuplicateArrayElements(nowtabdata);
			for (k = 0; k < tabdup.length; k++) {	
				$("#datab td:nth-child(2)").each(function () {
					if ($(this).text() === tabdup[k]) {
						$(this).css("color", "red");
						}	
					})
				}	
			})
			
		})
  
	})
  
  
//AFFICHER ID

$("#idcheck").click(function(){
	if($(this).is(":checked")) {
		interactcontrol();
		vecsource.forEachFeature( function(f2) { 
			if ( f2.getGeometry().getType() == "Polygon"  || f2.getGeometry().getType() == "MultiPolygon") {
				f2.setStyle(
					function (fiture) { 
						if (fiture.getGeometry().getType() == "Polygon" || fiture.getGeometry().getType() == "MultiPolygon"  ) {
							tstyle.getText().setText(String(fiture.getId()));
							return polstylid;
							}
						})
					}
				});
			}
	else {
		interactcontrol();
		vecsource.forEachFeature( function(remfit) { 
		if (remfit.getGeometry().getType() == "Polygon" || remfit.getGeometry().getType() == "MultiPolygon"  ) {
				remfit.setStyle(polstyle);
				}
			})
		}
	})


//CLEAR

$("#unselect").click(function(){
	selectInteraction.getFeatures().clear();
	selectMulti.getFeatures().clear();
	selectpop.getFeatures().clear();
	interactcontrol();
	$("#unselect").removeClass("btn-outline-secondary");
	});


//MESURE

$("#mesuretool").click(function(){
	interactcontrol();
	$("#mesuretool").removeClass("btn-outline-secondary");
	var sketch;
	var measureTooltipElement;
	var measureTooltip;
	var formatLength = function(line) {
		var length;
		var coordmes = line.getCoordinates();
		length = 0;
		var sourceProj = map.getView().getProjection();
		for (var i = 0, ii = coordmes.length - 1; i < ii; ++i) {
			var c1 = ol.proj.transform(coordmes[i], sourceProj, 'EPSG:4326');
			var c2 = ol.proj.transform(coordmes[i + 1], sourceProj, 'EPSG:4326');
			length = ol.sphere.getDistance(c1, c2);
			}
		var output;
		if (length > 1000) {
			output = (Math.round(length / 1000 * 100) / 100) +' ' + 'km';} 
		else {output = (Math.round(length * 100) / 100) + ' ' + 'm';}
		return output;
		};
	drawmesure.setActive(true);
	createMeasureTooltip();
	var listener;
	drawmesure.on('drawstart', function(evt) {
		sketch = evt.feature;
		var tooltipCoord = evt.coordinate;
		listener = sketch.getGeometry().on('change', function(evt) {
			var geom = evt.target;
			var output;
			output = formatLength(geom);
			tooltipCoord = geom.getLastCoordinate();
			measureTooltipElement.innerHTML = output;
			measureTooltip.setPosition(tooltipCoord);
			});
		});
	drawmesure.on('drawend', function() {
		measureTooltipElement.className = 'ol-tooltip ol-tooltip-static';
		measureTooltip.setOffset([0, -7]);
		sketch = null;
		createMeasureTooltip();
		});

	function createMeasureTooltip() {
		if (measureTooltipElement) { measureTooltipElement.parentNode.removeChild(measureTooltipElement);}
		measureTooltipElement = document.createElement('div');
		measureTooltipElement.className = 'ol-tooltip ol-tooltip-measure';
		measureTooltip = new ol.Overlay({
			element: measureTooltipElement,
			offset: [0, -15],
			positioning: 'bottom-center'
			});
		map.addOverlay(measureTooltip);
		}
	});
		

//MOVE
	
$("#movetool").click(function(){
	interactcontrol();
	$("#movetool").removeClass("btn-outline-secondary");
	featureDrager.setActive(true);
	snap.setActive(true);
	});
	

//MODIFIER
		
$("#modif").click(function(){
	interactcontrol();
	$("#modif").removeClass("btn-outline-secondary");
	selectInteraction.setActive(true);
	selectInteraction.on('select', function(eventmod) {
		if (eventmod.selected[0].getGeometry().getType() == "Circle") {
			modi.setActive(false);
			} 
		else {
			modi.setActive(true);
			snap.setActive(true);
			}
		})
	});
	

//DESSIN STANDART

$("#standart").click(function(){
	interactcontrol();
	$("#standart").removeClass("btn-outline-secondary");
	drawi.setActive(true);
	snap.setActive(true);
	});


//DESSIN RECTANGLE

$("#dessin_ortho").click(function(){
	interactcontrol();
	$("#dessin_ortho").removeClass("btn-outline-secondary");
	drawortho.setActive(true);
	snap.setActive(true);
	}); 


//POINT

$("#pointool").click( function() {
	interactcontrol();
	$("#pointool").removeClass("btn-outline-secondary");
	drawpoint.setActive(true);
	snap.setActive(true);
	});


//LINE

$("#linetool").click( function() {
	interactcontrol();
	$("#linetool").removeClass("btn-outline-secondary");
	drawline.setActive(true);
	snap.setActive(true);
	});

	
//ROTATION
	
$("#rotate").click(function(){
	interactcontrol();
	$("#rotate").removeClass("btn-outline-secondary");
	snap.setActive(true);
	itransform.setActive(true);
	});


//COPIERCOLLER
		
$("#cpp").click(function(){
	interactcontrol();
	$("#cpp").removeClass("btn-outline-secondary");
	var selectSingleClick = new ol.interaction.Select();
	map.addInteraction(selectSingleClick);
	selectSingleClick.on('select', function(event) {
		var copgeo = event.selected[0].getGeometry().getCoordinates();
		if (event.selected[0].getGeometry().getType() == "Polygon") {
			copol = new ol.Feature({
							type: 'Polygon',
							geometry: new ol.geom.Polygon(copgeo)
							});		
					}	
		else if (event.selected[0].getGeometry().getType() == "MultiPolygon") {
			copol = new ol.Feature({
						type: 'MultiPolygon',
						geometry: new ol.geom.MultiPolygon(copgeo)
						});			
				}
			else if (event.selected[0].getGeometry().getType() == "LineString") {
				copol = new ol.Feature({
						type: 'LineString',
						geometry: new ol.geom.LineString(copgeo)
						});			
				}
			else  {bootbox.alert('Géométrie non prise en charge');}	
			copol.setId('cop' + copol.ol_uid);
			vecsource.addFeature(copol);
			selectSingleClick.getFeatures().clear();
			map.removeInteraction(selectSingleClick);
		})
	})


//GRID

$("#gr").click(function(){
	interactcontrol();
	$("#gr").removeClass("btn-outline-secondary");
	var ncol = $("#colnumber").val();
	var nline = $("#linenumber").val();
	if (parseInt(ncol) >= 0 && parseInt(nline) >= 0) {
		var npol = ncol*nline;
		interactcontrol();
		var selectSingleClick = new ol.interaction.Select();
		map.addInteraction(selectSingleClick);
		selectSingleClick.on('select', function(event) {
			if (event.selected[0].getGeometry().getType() == "Polygon") {
				var convgeo = event.selected[0].getGeometry();
				var conv84 = convgeo.clone().transform('EPSG:3857', 'EPSG:4326');
				var geodat = conv84.getCoordinates();
				var gridid = event.selected[0].getGeometry().ol_uid;
				vecsource.removeFeature(event.selected[0]);
				var p1 =geodat[0][0];
				var p2= geodat[0][1];
				var p3 = geodat[0][2];
				var p4 = geodat[0][3];	
				//VEC P4P1
				var a1x = (p4[0] - p1[0]);
				var a1y = (p4[1] - p1[1]);
				//VEC P2 P3
				var c1x = (p3[0] - p2[0]);
				var c1y = (p3[1] - p2[1]);
				var a1b = [(1/nline)*(a1x) + p1[0], (1/nline)*(a1y) + p1[1]];
				var c1b = [(1/nline)*(c1x) + p2[0], (1/nline)*(c1y) + p2[1]];
				for (l = 0; l < nline; l++) {
					var b1x = (p2[0] - p1[0]);
					var b1y = (p2[1] - p1[1]);
					//P1P4 LIG 1
					var a1 = [(1/nline)*(a1x) + p1[0], (1/nline)*(a1y) + p1[1]];
					//P2P3 LIG 1 bis
					var c1 = [(1/nline)*(c1x) + p2[0], (1/nline)*(c1y) + p2[1]];
					//VEC A1C1
					var b2x = (c1[0] - a1[0]);
					var b2y = (c1[1] - a1[1]);
					var a2 = [a1[0], a1[1]];
					var c2 = [c1[0],c1[1]];
						for (k = 0; k < ncol; k++) {
							var datagrid = [];
							var multipol=[];
							var b1 = [(1/ncol)*(b1x) + p1[0], (1/ncol)*(b1y) + p1[1]];
							var b2 = [(1/ncol)*(b2x) + a1[0], (1/ncol)*(b2y) + a1[1]];
							datagrid = [p1,b1,b2,a1,p1];
							multipol = new ol.Feature({
								type: 'Polygon',
								geometry: new ol.geom.Polygon([datagrid])
								});
							multipol.getGeometry().transform('EPSG:4326', 'EPSG:3857');
							vecsource.addFeature(multipol);
							multipol.setId('gr'+multipol.ol_uid);
							p1 = b1;
							a1 = b2;
						}
					p1 = a2;
					p2 = b2;
					}
				selectSingleClick.getFeatures().clear();
				map.removeInteraction(selectSingleClick);
				} else {
					selectSingleClick.getFeatures().clear();
					map.removeInteraction(selectSingleClick);
					bootbox.alert('Choisir un paralèlogramme');
					}
			});	
		} else {bootbox.alert("valeurs entières");}
	});


//CONSTRUCTION

$("#selectunion").click(function(){
	interactcontrol();
	$("#selectunion").removeClass("btn-outline-secondary");
	selectMulti.setActive(true);
	var unionturf = [];
	var selected_features = [];
	var selected_feature_id = [];
	var selected_feature_id2 = [];
	var format = [];
	$("#union").prop("disabled",false);
	$("#multi").prop("disabled",false);
	$("#efface").prop("disabled",false);
	$("#union").click(function(){
		var a1 = [];
		var a2 = [];
		if (selectMulti.getFeatures().getLength() > 1 && selectMulti.getFeatures().getLength() < 3 ) {
			format = new ol.format.GeoJSON();
			a1 = turf.polygon(selectMulti.getFeatures().item(0).getGeometry().getCoordinates());
			a2 = turf.polygon(selectMulti.getFeatures().item(1).getGeometry().getCoordinates());
			unionturf = turf.union(a1,a2);
			selected_features = selectMulti.getFeatures();
			selected_feature_id = selected_features.item(0).getId();
			selected_feature_id2 = selected_features.item(1).getId();
			if (selected_feature_id = "undefined") {
				selected_features.item(0).setId('rec' + selected_features.item(0).ol_uid);
				selected_feature_id = selected_features.item(0).getId();
				}
			if (selected_feature_id2 = "undefined") {
				selected_features.item(1).setId('rec' + selected_features.item(1).ol_uid);
				selected_feature_id2 = selected_features.item(1).getId();
				}
			vecsource.removeFeature(selectMulti.getFeatures().item(0));
			vecsource.removeFeature(selectMulti.getFeatures().item(1));
			var upol = format.readFeature(unionturf);
			upol.setId('u' + upol.ol_uid);
			vecsource.addFeature(upol);
			selectMulti.getFeatures().clear();
			$("#union").prop("disabled",true);
			$("#multi").prop("disabled",true);
			$("#efface").prop("disabled",true);
			}
		});
		
	$("#multi").click(function(){
		$("#multi").removeClass("btn-outline-secondary");
		var numselect = selectMulti.getFeatures().getLength();
		var tabselect= [];
		if (selectMulti.getFeatures().getLength() > 1 ) {
			var mstok = [];
			for (i = 0; i < numselect; i++) {
				var mstok = selectMulti.getFeatures().item(i).getGeometry().getCoordinates().length;
				if (mstok > 1) {
					for (j = 0; j < mstok; j++){
						tabselect.push(selectMulti.getFeatures().item(i).getGeometry().getCoordinates()[j])
						}
					}
				else {
					tabselect.push(selectMulti.getFeatures().item(i).getGeometry().getCoordinates());
					}
				var multipolId = selectMulti.getFeatures().item(0).getId();			
				vecsource.removeFeature(selectMulti.getFeatures().item(i));		
			}
			multipoli = new ol.Feature({
				type: 'MultiPolygon',
				geometry: new ol.geom.MultiPolygon(tabselect)
					});
			multipoli.setId(multipolId);
			vecsource.addFeatures([multipoli]);
			selectMulti.getFeatures().clear();
			$("#union").prop("disabled",true);
			$("#multi").prop("disabled",true);
			$("#efface").prop("disabled",true);
			}
		});
		
	$("#efface").click( function() {
		$("#efface").removeClass("btn-outline-secondary");
		var numdel = selectMulti.getFeatures().getLength();
		if (selectMulti.getFeatures().getLength() > 0 ) {
			for (i = 0; i < numdel; i++) {		
				vecsource.removeFeature(selectMulti.getFeatures().item(i));
				}
			selectMulti.getFeatures().clear();
			$("#efface").prop("disabled",true);
			$("#union").prop("disabled",true);
			$("#multi").prop("disabled",true);
			}
		})
	
	})


//UNIMARC 123 COORDONNEES BBOX

/*
$("#gobox").click(function(){
	boxmodal.style.display = "block";
	var span = document.getElementsByClassName("close3")[0];
	span.onclick = function() {
		boxmodal.style.display = "none";
		$("#boxcontenu").empty();
		}
	window.onclick = function(event) {
		if (event.target == boxmodal) { 
			boxmodal.style.display = "none";
			$("#boxcontenu").empty();}
			}
	if (vecsource.getFeatures().length > 0 ) {
		
		
		
		
		
		var boxtotal = ol.extent.createEmpty();
		vecsource.forEachFeature( function(feature) {
			if ( feature.getGeometry().getType() == "Polygon" || feature.getGeometry().getType() == "MultiPolygon") {
				ol.extent.extend(boxtotal, feature.getGeometry().getExtent());
					}
				})
		var boxtotal1 = new ol.proj.toLonLat([boxtotal[0], boxtotal[1]]);
		var boxtotal2 = new ol.proj.toLonLat([boxtotal[2], boxtotal[3]]);
		
		//console.log(box1);
		//console.log(box2);
	
		
		var a10 = ol.coordinate.toStringHDMS(boxtotal1);
		var b10 = ol.coordinate.toStringHDMS(boxtotal2);
		a11 = a10.split(" ");
		b11 = b10.split(" ");

		if (boxtotal1[0] == 0) {
			a11.push("E");
			}
		if (boxtotal1[1] == 0) {
			a11.splice(3,0,"N");
			}
		if (boxtotal2[0] == 0) {
			b11.push("E");
			}
		if (boxtotal2[1] == 0) {
			b11.splice(3,0,"N");
			}

		for (i = 0; i < 3; i++) {
			a11[i] = a11[i].substring(0, a11[i].length -1);
				}
		for (i = 4; i < 7; i++) {
			a11[i] = a11[i].substring(0, a11[i].length -1);
				}
		if (a11[0].length == 2) {a11[0] = "0"+a11[0];}
		else if (a11[0].length == 1) {a11[0] = "00"+a11[0]};
		if (a11[4].length == 2) {a11[4] = "0"+a11[4];}
		else if (a11[4].length == 1) {a11[4] = "00"+a11[4]};
		lsud = a11[3]+a11[0]+a11[1]+a11[2];
		louest = a11[7]+a11[4]+a11[5]+a11[6];
		for (i = 0; i < 3; i++) {
			b11[i] = b11[i].substring(0, b11[i].length -1);
				}
		for (i = 4; i < 7; i++) {
			b11[i] = b11[i].substring(0, b11[i].length -1);
				}
		if (b11[0].length == 2) {b11[0] = "0"+b11[0];}
		else if (b11[0].length == 1) {b11[0] = "00"+b11[0]};
		if (b11[4].length == 2) {b11[4] = "0"+b11[4];}
		else if (b11[4].length == 1) {b11[4] = "00"+b11[4]};
		lnord = b11[3]+b11[0]+b11[1]+b11[2];
		lest = b11[7]+b11[4]+b11[5]+b11[6];
		$("#boxcontenu").append('<div>123 $d'+louest+' $e'+lest+' $f'+lnord+' $g'+lsud+'</div>');
		}
	})

*/


//UNIMARC 123 COORDONNEES BBOX

$("#gobox").click(function(){
	boxmodal.style.display = "block";
	var span = document.getElementsByClassName("close3")[0];
	span.onclick = function() {
		boxmodal.style.display = "none";
		$("#boxcontenu").empty();
		$("#boxcontenu1").empty();
		}
	window.onclick = function(event) {
		if (event.target == boxmodal) { 
			boxmodal.style.display = "none";
			$("#boxcontenu").empty();
			$("#boxcontenu1").empty();
				}
			}
	if (vecsource.getFeatures().length > 0 ) {
		
		// TOUS LES POLYGONES
		
		var boxtotal = ol.extent.createEmpty();
		vecsource.forEachFeature( function(featuretotal) {
			if ( featuretotal.getGeometry().getType() == "Polygon" || featuretotal.getGeometry().getType() == "MultiPolygon") {
				ol.extent.extend(boxtotal, featuretotal.getGeometry().getExtent());
					}
				})
		var boxtotal1 = new ol.proj.toLonLat([boxtotal[0], boxtotal[1]]);
		var boxtotal2 = new ol.proj.toLonLat([boxtotal[2], boxtotal[3]]);
		var a10 = ol.coordinate.toStringHDMS(boxtotal1);
		var b10 = ol.coordinate.toStringHDMS(boxtotal2);
		a11 = a10.split(" ");
		b11 = b10.split(" ");

		if (boxtotal1[0] == 0) {
			a11.push("E");
			}
		if (boxtotal1[1] == 0) {
			a11.splice(3,0,"N");
			}
		if (boxtotal2[0] == 0) {
			b11.push("E");
			}
		if (boxtotal2[1] == 0) {
			b11.splice(3,0,"N");
			}

		for (i = 0; i < 3; i++) {
			a11[i] = a11[i].substring(0, a11[i].length -1);
				}
		for (i = 4; i < 7; i++) {
			a11[i] = a11[i].substring(0, a11[i].length -1);
				}
		if (a11[0].length == 2) {a11[0] = "0"+a11[0];}
		else if (a11[0].length == 1) {a11[0] = "00"+a11[0]};
		if (a11[4].length == 2) {a11[4] = "0"+a11[4];}
		else if (a11[4].length == 1) {a11[4] = "00"+a11[4]};
		lsud = a11[3]+a11[0]+a11[1]+a11[2];
		louest = a11[7]+a11[4]+a11[5]+a11[6];
		for (i = 0; i < 3; i++) {
			b11[i] = b11[i].substring(0, b11[i].length -1);
				}
		for (i = 4; i < 7; i++) {
			b11[i] = b11[i].substring(0, b11[i].length -1);
				}
		if (b11[0].length == 2) {b11[0] = "0"+b11[0];}
		else if (b11[0].length == 1) {b11[0] = "00"+b11[0]};
		if (b11[4].length == 2) {b11[4] = "0"+b11[4];}
		else if (b11[4].length == 1) {b11[4] = "00"+b11[4]};
		lnord = b11[3]+b11[0]+b11[1]+b11[2];
		lest = b11[7]+b11[4]+b11[5]+b11[6];
		$("#boxcontenu").append('<div>Total $d '+louest+' $e '+lest+' $f '+lnord+' $g '+lsud+'</div>');
		
		// CHAQUE POLYGONE
		
		vecsource.forEachFeature( function(feature) {
			if ( feature.getGeometry().getType() == "Polygon" || feature.getGeometry().getType() == "MultiPolygon") {
				
				var c1 = feature.getGeometry().getExtent();
				
				var box1 = new ol.proj.toLonLat([c1[0], c1[1]]);
				var box2 = new ol.proj.toLonLat([c1[2], c1[3]]);
				var boxpolID = feature.getId();
				
				var a = ol.coordinate.toStringHDMS(box1);
				var b = ol.coordinate.toStringHDMS(box2);
				a1 = a.split(" ");
				b1 = b.split(" ");

				if (box1[0] == 0) {
					a1.push("E");
					}
				if (box1[1] == 0) {
					a1.splice(3,0,"N");
					}
				if (box2[0] == 0) {
					b1.push("E");
					}
				if (box2[1] == 0) {
					b1.splice(3,0,"N");
					}

				for (i = 0; i < 3; i++) {
					a1[i] = a1[i].substring(0, a1[i].length -1);
					}
				for (i = 4; i < 7; i++) {
					a1[i] = a1[i].substring(0, a1[i].length -1);
					}
				if (a1[0].length == 2) {a1[0] = "0"+a1[0];}
				else if (a1[0].length == 1) {a1[0] = "00"+a1[0]};
				if (a1[4].length == 2) {a1[4] = "0"+a1[4];}
				else if (a1[4].length == 1) {a1[4] = "00"+a1[4]};
				lsud = a1[3]+a1[0]+a1[1]+a1[2];
				louest = a1[7]+a1[4]+a1[5]+a1[6];
				for (i = 0; i < 3; i++) {
					b1[i] = b1[i].substring(0, b1[i].length -1);
					}
				for (i = 4; i < 7; i++) {
					b1[i] = b1[i].substring(0, b1[i].length -1);
					}
				if (b1[0].length == 2) {b1[0] = "0"+b1[0];}
				else if (b1[0].length == 1) {b1[0] = "00"+b1[0]};
				if (b1[4].length == 2) {b1[4] = "0"+b1[4];}
				else if (b1[4].length == 1) {b1[4] = "00"+b1[4]};
				lnord = b1[3]+b1[0]+b1[1]+b1[2];
				lest = b1[7]+b1[4]+b1[5]+b1[6];
				$("#boxcontenu1").append('<div>'+ boxpolID +' $d '+louest+' $e '+lest+' $f '+lnord+' $g '+lsud+'</div>');
				}
			})
		}
	})

//MARC 21 COORDONNEES BBOX

$("#gobox21").click(function(){
	boxmodal2.style.display = "block";
	var span = document.getElementsByClassName("close4")[0];
	span.onclick = function() {
		boxmodal2.style.display = "none";
		$("#box2contenu").empty();
		$("#box3contenu").empty();
		}
	window.onclick = function(event) {
		if (event.target == boxmodal2) { 
			boxmodal2.style.display = "none";
			$("#box2contenu").empty();
			$("#box3contenu").empty();
				}
			}
	if (vecsource.getFeatures().length > 0 ) {
		
		
		var boxtotal = ol.extent.createEmpty();
		vecsource.forEachFeature( function(featuretotal) {
			if ( featuretotal.getGeometry().getType() == "Polygon" || featuretotal.getGeometry().getType() == "MultiPolygon") {
				ol.extent.extend(boxtotal, featuretotal.getGeometry().getExtent());
					}
				})
		var boxtotal1 = new ol.proj.toLonLat([boxtotal[0], boxtotal[1]]);
		var boxtotal2 = new ol.proj.toLonLat([boxtotal[2], boxtotal[3]]);
		var dt = boxtotal1[0].toFixed(6);
		var et = boxtotal2[0].toFixed(6);
		var ft = boxtotal2[1].toFixed(6);
		var gt = boxtotal1[1].toFixed(6);
				
		if (dt < 100 && dt >= 10 ) {
			dt = '0' + String(dt);
			}
				
		if (dt < 10 && dt >= 0 ) {
			dt = '00' + String(dt);
			}
				
		if (et < 100 && et >= 10 ) {
			et = '0' + String(et);
			}
				
		if (et < 10 && et >= 0 ) {
			et = '00' + String(et);
			}
				
		if (ft < 100 && ft >= 10 ) {
			ft = '0' + String(ft);
			}
				
		if (ft < 10 && ft >= 0 ) {
			ft = '00' + String(ft);
		}
				
		if (gt < 100 && gt >= 10 ) {
			gt = '0' + String(gt);
			}
				
		if (gt < 10 && gt >= 0 ) {
			gt = '00' + String(gt);
		}
				
		if (dt <= -10 & dt > -100) {
			dt = '-0' + String(Math.abs(dt));
			}
				
		if (dt < 0 && dt > -10 ) {
			dt = '-00' + String(Math.abs(dt));
			}
			
		if (et <= -10 & et > -100) {
			et = '-0' + String(Math.abs(et));
			}
				
		if (et < 0 && et > -10 ) {
			et = '-00' + String(Math.abs(et));
			}
				
		if (ft <= -10 & ft > -100) {
			ft = '-0' + String(Math.abs(ft));
			}
				
		if (ft < 0 && ft > -10 ) {
			ft = '-00' + String(Math.abs(ft));
			}
				
		if (gt <= -10 & gt > -100) {
			gt = '-0' + String(Math.abs(gt));
			}
				
		if (gt < 0 && gt > -10 ) {
			gt = '-00' + String(Math.abs(gt));
			}
				
		$("#box2contenu").append('<div>Total ‡d ' + dt + ' ‡e ' + et + ' ‡f ' + ft + ' ‡g ' + gt + '</div>');

		
		vecsource.forEachFeature( function(feature) {
			if ( feature.getGeometry().getType() == "Polygon" || feature.getGeometry().getType() == "MultiPolygon") {
				var c1 = feature.getGeometry().getExtent();
				var box1 = new ol.proj.toLonLat([c1[0], c1[1]]);
				var box2 = new ol.proj.toLonLat([c1[2], c1[3]]);
				var boxpolID = feature.getId();
				var d = box1[0].toFixed(6);
				var e = box2[0].toFixed(6);
				var f = box2[1].toFixed(6);
				var g = box1[1].toFixed(6);
				
				if (d < 100 && d >= 10 ) {
					d = '0' + String(d);
				}
				
				if (d < 10 && d >= 0 ) {
					d = '00' + String(d);
				}
				
			
				if (e < 100 && e >= 10 ) {
					e = '0' + String(e);
				}
				
				if (e < 10 && e >= 0 ) {
					e = '00' + String(e);
				}
				
				if (f < 100 && f >= 10 ) {
					f = '0' + String(f);
				}
				
				if (f < 10 && f >= 0 ) {
					f = '00' + String(f);
				}
				
				if (g < 100 && g >= 10 ) {
					g = '0' + String(g);
				}
				
				if (g < 10 && g >= 0 ) {
					g = '00' + String(g);
				}
				
				
				if (d <= -10 & d > -100) {
					d = '-0' + String(Math.abs(d));
				}
				
				if (d < 0 && d > -10 ) {
					d = '-00' + String(Math.abs(d));
				}
			
				if (e <= -10 & e > -100) {
					e = '-0' + String(Math.abs(e));
				}
				
				if (e < 0 && e > -10 ) {
					e = '-00' + String(Math.abs(e));
				}
				
				if (f <= -10 & f > -100) {
					f = '-0' + String(Math.abs(f));
				}
				
				if (f < 0 && f > -10 ) {
					f = '-00' + String(Math.abs(f));
				}
				
				if (g <= -10 & g > -100) {
					g = '-0' + String(Math.abs(g));
				}
				
				if (g < 0 && g > -10 ) {
					g = '-00' + String(Math.abs(g));
				}
				
				$("#box3contenu").append('<div>'+ boxpolID +' ‡d ' + d + ' ‡e ' + e + ' ‡f ' + f + ' ‡g ' + g + '</div>');

				}
			})

		}
	})


//TOUT EFFACER

$("#resetall").click(function(){
	interactcontrol();
	vecsource.forEachFeature(function(rmlayer) {
		vecsource.removeFeature(rmlayer);
		})
	})


//DOWNLOAD GEOJSON

$("#fvector").click(function(){
	gmodal.style.display = "block";
	var span = document.getElementsByClassName("close1")[0];
	span.onclick = function() {
		gmodal.style.display = "none";
		}
	window.onclick = function(event) {
		if (event.target == gmodal) { gmodal.style.display = "none";}
		}
	$(document).on('change','.btn-file :file', function() {
		var input = $(this),
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
			});
	$('.btn-file :file').on('fileselect', function(event, label) {
		var input = $(this).parents('.input-group').find(':text');
		log = label;
		input.val(log)
		});
	})

$('#gupload').on('click', function () {
	var fileInput = document.getElementById('hFichier');
	var strc = fileInput.value;
	if (strc.substring(strc.length - 8, strc.length) == '.geojson') {
		var file = fileInput.files[0];
		var reader = new FileReader();
		reader.onload = function(e) {
			var content = JSON.parse(reader.result);
			var geoclient = new ol.format.GeoJSON( {featureProjection: 'EPSG:3857'}).readFeatures( content );
			geoclient.forEach(function(feature){ 
				feature.setId(feature.get("ID_CM"));
				if (typeof(feature.getId()) == 'undefined') {
					feature.setId("rec"+feature.ol_uid);
						}
					});
			vecsource.addFeatures(geoclient);
			$("#fgjson").val('');
			gmodal.style.display = "none";
				}
		reader.readAsText(file);
		}
	else {bootbox.alert('Format .geojson Projection 4326');}
	})


// EXPORT

var geomchoix = document.getElementById('exportgeom');
$("#save").prop("disabled",true);
$("#saveshp").prop("disabled",true);
$("#savekml").prop("disabled",true);

geomchoix.addEventListener('change', function() {
	if (this.value == 'geometrie') {
		$("#save").prop("disabled",true);
		$("#saveshp").prop("disabled",true);
		$("#savekml").prop("disabled",true);
		}
	else if (this.value == 'polygon') {
		$("#save").prop("disabled",false);
		$("#saveshp").prop("disabled",false);
		$("#savekml").prop("disabled",false);
		}
	else if (this.value == 'point') {
		$("#save").prop("disabled",false);
		$("#saveshp").prop("disabled",true);
		$("#savekml").prop("disabled",true);
		}
	else if(this.value == 'ligne') {
		$("#save").prop("disabled",false);
		$("#saveshp").prop("disabled",true);
		$("#savekml").prop("disabled",true);
		}
	})


//EXPORT GEOJSON

$("#save").click( function() {
	if (geomchoix.value == 'polygon') {
		var geom = [];
		vecsource.forEachFeature( function(feature) {
			if ( feature.getGeometry().getType() == "Polygon" || feature.getGeometry().getType() == "MultiPolygon") {
				//geom.push(new ol.Feature(feature/*.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326')*/));
				//IDSERIE
				//var serid = document.getElementById('idserie').value;
				/*if (serid.length > 0) {
					var fid = feature.getId();
					feature.setProperties({'ID_CM':''+ serid + '_' + fid});
					}*/
				geom.push(feature);
				}
			})
		var writer = new ol.format.GeoJSON();
		var geoJsonStr = writer.writeFeatures(geom, {featureProjection: 'EPSG:3857'});
		var filename = "polygon";
		var blob = new Blob([geoJsonStr], {type: "text/plain;charset=utf-8"});
		saveAs(blob, filename+".geojson");
		}
	else if (geomchoix.value == 'point') {
		var geom = [];
		vecsource.forEachFeature( function(feature) {
			if ( feature.getGeometry().getType() == "Point") {
				//geom.push(new ol.Feature(feature/*.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326')*/));
				geom.push(feature);
				}
			})
		var writer = new ol.format.GeoJSON();
		var geoJsonStr = writer.writeFeatures(geom, {featureProjection: 'EPSG:3857'});
		var filename = "point";
		var blob = new Blob([geoJsonStr], {type: "text/plain;charset=utf-8"});
		saveAs(blob, filename+".geojson");
		}
	else if (geomchoix.value == 'ligne') {
			var geom = [];
			vecsource.forEachFeature( function(feature) {
				if ( feature.getGeometry().getType() == "LineString") {
				//geom.push(new ol.Feature(feature/*.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326')*/));
				geom.push(feature);
				}
			})
		var writer = new ol.format.GeoJSON();
		var geoJsonStr = writer.writeFeatures(geom, {featureProjection: 'EPSG:3857'});
		var filename = "line";
		var blob = new Blob([geoJsonStr], {type: "text/plain;charset=utf-8"});
		saveAs(blob, filename+".geojson");
		}
	});


//EXPORT KML

$("#savekml").click( function() {
	var geom = [];
	vecsource.forEachFeature( function(feature) {
	if ( feature.getGeometry().getType() == "Polygon" || feature.getGeometry().getType() == "MultiPolygon") {
		//geom.push(new ol.Feature(feature.getGeometry().clone().transform('EPSG:3857', 'EPSG:4326')));
		//IDSERIE
		/*var serid = document.getElementById('idserie').value;
		if (serid.length > 0) {
				var fid = feature.getId();
				feature.setProperties({'ID_CM':''+ serid + '_' + fid});
				}*/
		geom.push(feature);
		}
	});
var format = new ol.format.KML();
    var kml = format.writeFeatures(geom, {featureProjection: 'EPSG:3857'});
	var filename = "exportkml";
	var blob = new Blob([kml], {type: "text/plain;charset=utf-8"});
	saveAs(blob, filename+".kml");
	});


//EXPORT SHP

$("#saveshp").click( function() {
			var fname = 'exportshp';
			var geomp = [];
			var geomm = [];
			var idcmp = [];
			var idcmm = [];
			var polsolo = [];
			var formatwkt = new ol.format.WKT();
			var str = 'LINESTRING ';
			vecsource.forEachFeature( function(feature) {
				if ( feature.getGeometry().getType() == "Polygon") {
					//IDSERIE
					/*var serid = document.getElementById('idserie').value;
						if (serid.length > 0) {
							var fid = feature.getId();
							feature.setId(serid + '_' + fid);
						}*/
					if (typeof(feature.getId()) == "undefined") {feature.setId('rec' + feature.ol_uid);}
					idcmp.push(feature.getId());
					var v = formatwkt.writeGeometry(feature.getGeometry());
					var v0 = v.replace('POLYGON(', str);
					var polsolo = v0.replace('))',')');
					geomp.push(polsolo);
					}
				else if (feature.getGeometry().getType() == "MultiPolygon" ) {
					var multiple = [];
					//IDSERIE
					/*var serid = document.getElementById('idserie').value;
					if (serid.length > 0) {
						var fid = feature.getId();
						feature.setId(serid + '_' + fid);
						}*/
					if (typeof(feature.getId()) == "undefined") {feature.setId('rec' + feature.ol_uid);}
					idcmm.push(feature.getId());
					var w = formatwkt.writeGeometry(feature.getGeometry());
					var w0 = w.replace('MULTIPOLYGON((','');
					var w1 = w0.replace(')))',')');
					var w2 = w1.split('),(');
					for (i = 0; i < w2.length; i++) {
						multiple[i] = str.concat(w2[i]);
						}
					geomm.push(multiple);
					}
				})
		if (geomp.length == 0) {
				geomp ='vide';
				idcmp ='vide';
				}
		if (geomm.length == 0) {
				geomm ='vide';
				idcmm ='vide';
				}
		var polsum = [fname,idcmp, geomp, idcmm, geomm];
		if (polsum[1] != "vide" || polsum[4] != "vide") {
			$.ajax({
			type: "POST",
			url: "toshp.php",
			data: { tabgeom : polsum },
			success: function(response) {
				window.location = response;
				}
			})
		} else {console.log('stop');}
  }) 
 
  //END

});
